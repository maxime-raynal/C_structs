#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <time.h>
#include <stdlib.h>

#include "heap.h"

#define DEFAULT_TEST_SIZE 8000000


bool heap_test_cmp_func(int n1, int n2){
    return (n1 < n2);
}


int main(int argc, char * argv[]){
    int test_size;
    if (argc > 1){
        test_size = atoi(argv[1]);
    } else {
        test_size = DEFAULT_TEST_SIZE;
    }
    srand(time(NULL));

    int *T = malloc(test_size * sizeof(int));
    for (int i = 0; i < test_size; i++){
        T[i] = i;
    }

    int tmp, r;
    for (int i = 0; i < test_size; i++){
        r = rand() % ((int) test_size);
        tmp = T[i];
        T[i] = T[r];
        T[r] = tmp;
    }

    heap_t *heap = heap_init(heap_test_cmp_func);
    for (int i = 0; i < test_size; i++){
        assert(heap->nb_elem == i);
        heap_push(heap, T[i]);
    }

    for (int i = 0; i < (int) test_size; i++){
        assert(i == heap_pop(heap));
    }

    assert(heap->nb_elem == 0);
    free(T);
    heap_free(heap);
    return 0;
}
