#ifndef GENERIC_HEAP_DOT_H
#define GENERIC_HEAP_DOT_H

#include <stdlib.h>
#include <stdbool.h>

#define HEAP_INIT_SIZE 16

typedef int heap_data_t;

typedef bool heap_cmp_func(heap_data_t, heap_data_t);

typedef struct{
    heap_data_t *nodes;
    heap_cmp_func *compar;
    int alloc_size, nb_elem;
} heap_t;


heap_t *heap_init(heap_cmp_func *compar);

void heap_free(heap_t *heap);

void heap_push(heap_t *heap, heap_data_t data);

heap_data_t heap_pop(heap_t *heap);

#endif
